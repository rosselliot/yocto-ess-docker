# Dockerfile to generate a docker image for ESS Linux
FROM scratch
ADD cct-image-cct-64.tar /

ENV container docker

# Log directory needed for dnf package manager
RUN mkdir /var/volatile/log

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/lib/systemd/systemd"]
